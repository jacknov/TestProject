#ifndef FUNCTIONS_H
#define FUNCTIONS_H
//STL
#include <string>
#include <vector>
#include <map>
#include <iostream>
#include <iterator>
#include <algorithm>
#include <random>

namespace stuff {

//осуществляет проверку ввода и возвращает размер контейнера
template<typename Size>
Size input_size(const std::string& message) {
    Size s;
    std::cout << message;
    while(true) {
        std::cin >> s;
        if(std::cin.fail()) {
            std::cout << std::endl << "Non-numeric value. Try again: ";
            std::cin.clear();
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
        } else if(!s) {
            std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
            std::cout << std::endl << "Size must be greater then zero. Try again: ";
        } else {
            break;
        }
    }

    return s;
}
//выводит контейнер на консоль
template<typename Cont>
void output_cont(const Cont &c, const std::string &message) {
    std::cout << message;
    std::copy(std::begin(c), std::end(c),
              std::ostream_iterator<typename Cont::value_type>{std::cout, ", "});
    std::cout << std::endl;
}
//перегруженная версия для std::map
template<typename Key, typename Val>
void output_cont(const std::map<Key, Val> &m, const std::string &message) {
    std::cout << message;
    for(const auto& [pos, num] : m) {
        std::cout << "{" << pos << ", " << num << "}" << ", ";
    }
    std::cout << std::endl;
}
//функция удаления случайных элементов из контейнера
template<typename Cont, typename Val, typename RD>
void erase_random(Cont &c, Val count, RD &rd, const std::string &message) {
    std::vector<Val> del;
    std::uniform_int_distribution<Val> ud{0, c.size() - 1};
    Val num{0};
    //заполняем вектор позиций для удаления
    //случайными неповторяющимися значениями
    while(count > 0) {
        num = ud(rd);
        if(std::find(std::begin(del), std::end(del), num) == std::end(del)) {
            del.push_back(num);
            --count;
        }
    }
    //сортируем вектор позиций для удаления по убыванию
    //для упрощения дальнейшего поиска и удаления
    //этих позиций в контейнере
    std::sort(std::begin(del), std::end(del), std::greater<Val>{});
    output_cont(del, message);
    for(const auto& e : del) {
        Val counter{0};
        //алгоритм немного накладный, зато контейнер не обязан обладать
        //реверсивными итераторами
        for(auto it{std::begin(c)}; it != std::end(c); ++it) {
            if(counter == e) {
                c.erase(it);
                break;
            }
            ++counter;
        }
    }
}
//сравниваем значения исходного вектора с вектором пересечения
//если в векторе пересечения такого значения нет, то удаляем его
//из исходного вектора
template<typename Cont, typename Val>
void handle_container(Cont &c, const std::vector<Val> &sample) {
    auto it{std::begin(c)};
    while(it != std::end(c)) {
        if(std::find(std::begin(sample), std::end(sample), *it) ==
                std::end(sample)) {
            it = c.erase(it);
        } else {
            ++it;
        }
    }
}
//сравниваем значения исходной карты с вектором пересечения
//если в векторе пересечения такого значения нет, то удаляем его
//из исходной карты
template<typename Key, typename Val>
void handle_container(std::map<Key, Val> &m, const std::vector<Val> &sample) {
    auto it{std::begin(m)};
    while(it != std::end(m)) {
        if(std::find(std::begin(sample), std::end(sample), it->second) ==
                std::end(sample)) {
            it = m.erase(it);
        } else {
            ++it;
        }
    }
}
} // namespace stuff

#endif // FUNCTIONS_H
