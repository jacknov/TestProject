//Custom
#include "stuff.hpp"
//STL
#include <iostream>
#include <iterator>

using size = std::vector<int>::size_type;

int main()
{
    size size_v{0};
    //проверка ввода размера вектора
    size_v = stuff::input_size<size>("Input size of vector (integer > 0): ");
    //проверка ввода размера карты
    size size_m{0};
    size_m = stuff::input_size<size>("Input size of map (integer > 0): ");
    //создаем генератор недетерминированных случайных чисел для "посева"
    //начального значения
    ///в некоторых реализациях возвращает детерминированные случайные числа
    std::random_device rd;
    //создаем детерминированные генераторы случайных чисел для вектора и карты
    //и задаем им случайные начальные значения
    std::mt19937 mt{rd()};
    //создаем равномерное распределение от 1 до 9
    std::uniform_int_distribution<int> dist{1, 9};
    //создаем вектор и карту
    std::vector<int> v;
    std::map<size, int> m;
    //заполняем вектор
    v.reserve(size_v);
    auto gen{[&] { return dist(mt); }};
    std::generate_n(std::back_inserter(v), size_v, gen);
    //заполняем карту
    for (size i{0}; i < size_m; ++i) {
        auto val{gen()};
        m.insert({i, val});
    }
    //выводим вектор
    stuff::output_cont(v, "Vector: ");
    //выводим карту
    stuff::output_cont(m, "Map: ");
    //создаем равномерное распределение для количества удаленных элементов
    std::uniform_int_distribution<int> del_dist{0, 15};
    auto del_num{static_cast<size>(del_dist(mt))};
    //учитываем ситуацию, когда количество элементов,
    //требующих удаления, больше размера вектора
    del_num = std::min(size_v, del_num);
    std::cout << "Number of deleted elements in vector: " << del_num << std::endl;
    //удаляем случайное количество случайных элементов вектора
    stuff::erase_random(v, del_num, mt, "Positions of elements deleted from vector: ");
    del_num = static_cast<size>(del_dist(mt));
    //учитываем ситуацию, когда количество элементов,
    //требующих удаления, больше размера карты
    del_num = std::min(size_m, del_num);
    std::cout << "Number of deleted elements in map: " << del_num << std::endl;
    //удаляем случайное количество случайных элементов карты
    stuff::erase_random(m, del_num, mt, "Positions of elements deleted from map: ");

    //выводим вектор после удаления элементов
    stuff::output_cont(v, "Vector after deletion: ");
    //выводим карту после удаления элементов
    stuff::output_cont(m, "Map after deletion: ");
    //преобразуем карту в вектор для использования std::set_intersection
    std::vector<int> m_to_v;
    std::transform(std::begin(m), std::end(m),
                   std::back_inserter(m_to_v), [](const auto& pair){ return pair.second; });
    //выводим вектор, получившийся на основе карты
    stuff::output_cont(m_to_v, "Vector created from map: ");
    //перед использованием std::set_intersection векторы должны быть отсортированы
    auto v_sorted{v};
    std::sort(std::begin(m_to_v), std::end(m_to_v));
    std::sort(std::begin(v_sorted), std::end(v_sorted));
    //выводим отсортированные векторы
    stuff::output_cont(v_sorted, "Sorted vector: ");
    stuff::output_cont(m_to_v, "Sorted vector created from map: ");
    //создаем вектор пересечения
    std::vector<int> intersection;
    //применяем к отсортированным векторам std::set_intersection
    std::set_intersection(std::begin(m_to_v), std::end(m_to_v),
                          std::begin(v_sorted), std::end(v_sorted),
                          std::back_inserter(intersection));
    //выводим вектор пересечения
    stuff::output_cont(intersection, "Intersection vector: ");
    //удаляем из контейнеров элементы, не являющиеся общими
    stuff::handle_container(v, intersection);
    stuff::handle_container(m, intersection);
    //выводим вектор с общими элементами
    stuff::output_cont(v, "Vector after intersection: ");
    //выводим карту с общими элементами
    stuff::output_cont(m, "Map after intersection: ");

    return 0;
}
